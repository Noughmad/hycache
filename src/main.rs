use std::net::SocketAddr;

use axum::{http::Request, body::{Body, Bytes}, response::Response, Extension, Router, routing::get};
use anyhow::{Context, Result};
use hyper::{StatusCode, Uri, body::to_bytes};
use tokio::{self, signal};
use redis::AsyncCommands;

mod state;
mod rules;
mod parts;
use crate::state::State;

#[tokio::main]
async fn main() -> Result<()> {
    let app = Router::new()
        .route("*path", get(cache_handler).head(cache_handler).post(nocache_handler).delete(nocache_handler).put(nocache_handler))
        .layer(Extension(State::new().await?));

    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .with_graceful_shutdown(shutdown_signal())
        .await
        .context("Axum serve")?;

    Ok(())
}

async fn shutdown_signal() {
    let ctrl_c = async {
        signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }

    println!("signal received, starting graceful shutdown");
}

fn set_backend_uri(req: &mut Request<Body>, state: &State) -> Result<()> {
    let mut builder = Uri::builder();
    if let Some(scheme) = req.uri().scheme() {
        builder = builder.scheme(scheme.clone());
    }
    builder = builder.authority(state.site_url.clone());
    if let Some(path_and_query) = req.uri().path_and_query() {
        builder = builder.path_and_query(path_and_query.clone());
    }
    *req.uri_mut() = builder.build()?;

    Ok(())
}

async fn nocache_handler(req: Request<Body>, Extension(state): Extension<State>) -> Result<Response<Body>, StatusCode> {
    nocache(req, state).await.map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)
}

async fn nocache(mut req: Request<Body>, state: State) -> Result<Response<Body>> {
    set_backend_uri(&mut req, &state)?;    
    Ok(state.client.request(req).await?)
}

async fn cache_handler(req: Request<Body>, Extension(state): Extension<State>) -> Result<Response<Body>, StatusCode> {
    cache(req, state).await.map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)
}

async fn cache(mut req: Request<Body>, state: State) -> Result<Response<Body>> {
    if state.rules.should_cache(req.uri().path()) {
        // Find existing cache entry
        let mut redis = state.redis.get().await?;
        let key = String::from(req.uri().path());

        let cached: Option<Bytes> = redis.get(&key).await?;

        if let Some(cached) = cached {
            // An existing cache entry was found
            // we can just return it.
            return Ok(Response::builder().body(Body::from(cached))?)
        }

        set_backend_uri(&mut req, &state)?;    
        let response = state.client.request(req).await?;

        let (parts, body) = response.into_parts();

        let cached_parts: CachedParts = parts.into();

        let bytes = to_bytes(body).await?;
        redis.set(&key, &*bytes).await?;

        // Insert new cache entry
        Ok(Response::from_parts(parts, Body::from(bytes)))

    } else {
        nocache(req, state).await
    }
}