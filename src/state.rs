use std::env;

use anyhow::{Context, Result};
use hyper::{client::HttpConnector, Body};

use crate::rules::Rules;

type RedisPool = bb8::Pool<bb8_redis::RedisConnectionManager>;

#[derive(Clone, Debug)]
pub struct State {
    pub redis: RedisPool,
    pub site_url: String,
    pub client: hyper::Client<HttpConnector, Body>,
    pub rules: Rules,
}

impl State {
    pub async fn new() -> Result<Self> {
        let redis_env = env::var("CACHE_URL").context("Redis env")?;
        let redis_manager =
            bb8_redis::RedisConnectionManager::new(redis_env).context("Redis manager")?;
        let redis = bb8::Pool::builder()
            .build(redis_manager)
            .await
            .context("Redis pool")?;

        let site_url = env::var("SITE_URL").context("SITE_URL env")?;

        Ok(State {
            redis,
            site_url,
            client: hyper::Client::new(),
            rules: Rules::new(),
        })
    }
}
