use std::collections::HashMap;

use anyhow::Result;
use axum::{http::response::Parts, body::Body, response::Response};

#[derive(Clone, Debug, Serialize, Deserialize)]
struct CachedParts<'a> {
    pub status: u16,
    pub version: &'a str,
    pub headers: HashMap<&'a str, &'a [u8]>,
    pub body: &'a [u8],
}

impl<'a> From<(&'a Parts, &'a [u8])> for CachedParts<'a> {
    fn from(p: (&'a Parts, &'a [u8])) -> CachedParts<'a> {
        unimplemented!()
    }
}

impl<'a> CachedParts<'a> {
    pub fn from_response(parts: &'a Parts, body: &'a [u8]) -> Self {
        let mut cached = Self {
            status: parts.status.as_u16(),
            version: "1.1",
            headers: HashMap::new(),
            body,
        };

        for (name, value) in parts.headers {
            if let Some(name) = name {
                cached.headers.insert(name.as_str(), value.as_bytes());
            }
        }

        cached
    }

    pub fn into_response(self, body: Body) -> Result<Response<Body>> {
        let mut builder = Response::builder();

        builder = builder.status(self.status);

        for (name, value) in self.headers {
            builder.header(name, value);
        }

        Ok(builder.body(body)?)
    }
}
