#[derive(Clone, Debug)]
pub struct Rules {

}

impl Rules {
    pub fn new() -> Rules {
        Rules {}
    }

    pub fn should_cache(&self, path: &str) -> bool {
        match path {
            "/" => true,
            "/welcome/" => true,
            _ => false,
        }
    }
}